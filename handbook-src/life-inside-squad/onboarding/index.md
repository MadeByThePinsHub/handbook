---
title: Onboarding to the team
---
You got an invite from the team and you accepted it or filled up an application form and we got accepted you. Welcome to the team! So what do you do as an newbie?

## Too Long; Didn't Read

* Review our [team handbok](/) first.
* Open your welcome email from us. (This might take a little longer, depending on schedule.)
* [Read the pre-onboarding process here](https://onboarding.madebythepins.tk/pre-onboarding) before your first day.
* Follow the onbparding procedures, such as:
    * Accept any org invites in GitHub and GitLab SaaS. If using Launchpad, we may send you an invite too but we use GitHub and GitLab more.
    * Accept instance invite to our Bitwarden instance (our Vaultwarden container is at `vault.madebythepibs.tk`).

## The Long Text

### As usual: Reading da handbook

WIP

### Open your welcome email from `system-noreply@onboard.rtapp.tk`

!!! info "Want to see what's inside of it?"
    See this [sample message here](welcome-mail-example.md) if you want to take an sneak peek.

Onboarding emails are sent manually from Andrei Jiroh or our HR team, usually from `Onboarder <system-noreply@onboard.rtapp.tk>`. Click the link provided to start the onboarding process, usually it starts in ab
