FROM gitpod/workspace-full

USER gitpod

# Development tools for Gitpod workspaces :)
RUN brew update; brew upgrade; brew install gh shellcheck hadolint

## Install Python and use 3.x as system default. Last updated on 11-18-2021 by bumping pyver from 3.8.5.
RUN pyenv install 3.10.0; pyenv global 3.10.0

## Upgrade pip to latest version
RUN pip install --upgrade pip